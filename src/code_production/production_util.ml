open Ast
open X86_64

type localEnv = int Smap.t (* Position dans la stack par rapport à rbp *)

let (strings : (string, string) Hashtbl.t) = Hashtbl.create 128

let (typeToInt : (typ, int) Hashtbl.t) = Hashtbl.create 128

let (intToType : (int, typ) Hashtbl.t) = Hashtbl.create 128

let (globalVariables : (ident, string) Hashtbl.t) = Hashtbl.create 128

let functions = ref Sset.empty

let nbLabels = ref 0

let listOfTypes file =
  Any :: Nothing :: Int64 :: Bool :: String
  :: Smap.fold (fun s _ cur -> Structure s :: cur) file.structures []

let getNewLabel () =
  let id = ".L" ^ string_of_int !nbLabels in
  incr nbLabels;
  id

let getStringLabel s =
  try Hashtbl.find strings s
  with Not_found ->
    Hashtbl.add strings s
      (".Sprint_string" ^ string_of_int (Hashtbl.length strings));
    Hashtbl.find strings s

let getGlobalVariable ident =
  try Hashtbl.find globalVariables ident
  with Not_found ->
    Hashtbl.add globalVariables ident (".Gglobal_variable_" ^ ident);
    Hashtbl.find globalVariables ident

let getTypeInd typ =
  try Hashtbl.find typeToInt typ
  with Not_found ->
    Hashtbl.add typeToInt typ ((2 * Hashtbl.length typeToInt) + 2);
    Hashtbl.add intToType ((2 * Hashtbl.length intToType) + 2) typ;
    Hashtbl.find typeToInt typ

let getType = Hashtbl.find intToType

let checkDefined localEnv ident =
  ( try
      let pos = Smap.find ident localEnv in
      movq (ind ~ofs:pos rbp) !%rdi
    with Not_found -> movq (lab (getGlobalVariable ident)) !%rdi )
  ++ movq (ind rdi) !%rbx
  ++ andq (imm 1) !%rbx
  ++ jne "exit_brute"

(* returns index of type of variable *)
let calcType (localEnv : localEnv) (ident : ident) =
  checkDefined localEnv ident
  ++ ( try
         let pos = Smap.find ident localEnv in
         movq (ind ~ofs:pos rbp) !%rax
       with Not_found -> movq (lab (getGlobalVariable ident)) !%rax )
  ++ movq (ind rax) !%rax

(* Pointer to variable in rdi. uses rdi and rbx *)
let checkTypeCompatible typ =
  movq (ind rdi) !%rdi
  ++ movq !%rdi !%rbx
  ++ subq (imm (getTypeInd Any)) !%rdi
  ++ subq (imm (getTypeInd typ)) !%rbx
  ++ imulq !%rbx !%rdi
  ++ cmpq (imm 0) !%rdi
  ++ jne "exit_brute"

(* Returns a pointer, containing enough space for n 64 bit words, pointer in rax *)
let allocateBlock nb = movq (imm (8 * nb)) !%rdi ++ call "malloc"

let getVariable localEnv id =
  checkDefined localEnv id
  ++ call "copy_variable"

let isSmaller (f1 : typedFunction) (f2 : typedFunction) =
  let rec aux strict (l1 : param list) (l2 : param list) =
    match (l1, l2) with
    | [], [] -> strict
    | a :: p, b :: q ->
        (snd b.desc = Any || snd a.desc <> Any)
        && aux (strict || (snd b.desc = Any && snd a.desc <> Any)) p q
    | _ -> assert false
  in
  aux false f1.params f2.params

let findMinFunction compatibleFunctions =
  let rec aux before = function
    | [] -> raise Not_found
    | f :: q ->
        if List.for_all (isSmaller f) (before @ q) then f
        else aux (f :: before) q
  in
  aux [] compatibleFunctions

let load_int =
  label "load_int" ++ pushq !%rdi ++ allocateBlock 2 ++ popq rcx
  ++ movq (imm (getTypeInd Int64)) !%rbx
  ++ movq !%rbx (ind rax)
  ++ movq !%rcx (ind ~ofs:8 rax)
  ++ ret

let load_bool =
  label "load_bool" ++ pushq !%rdi ++ allocateBlock 2 ++ popq rcx
  ++ movq (imm (getTypeInd Bool)) !%rbx
  ++ movq !%rbx (ind rax)
  ++ movq !%rcx (ind ~ofs:8 rax)
  ++ ret

let print_int =
  label "print_int"
  ++ subq (imm 8) !%rsp
  ++ movq (reg rdi) (reg rsi)
  ++ leaq (lab ".Sprint_int") rdi
  ++ xorq !%rax !%rax ++ call "printf"
  ++ addq (imm 8) !%rsp
  ++ ret

let print_bool =
  label "print_bool"
  ++ subq (imm 8) !%rsp
  ++ cmpq (imm 0) !%rdi
  ++ je ".Lfalse"
  ++ leaq (lab ".Sprint_true") rdi
  ++ jmp ".Lprint" ++ label ".Lfalse"
  ++ leaq (lab ".Sprint_false") rdi
  ++ label ".Lprint" ++ xorq !%rax !%rax ++ call "printf"
  ++ addq (imm 8) !%rsp
  ++ ret

let print_string =
  label "print_string"
  ++ subq (imm 8) !%rsp
  ++ movq (reg rdi) (reg rsi)
  ++ leaq (lab ".Sprint_string") rdi
  ++ xorq !%rax !%rax ++ call "printf"
  ++ addq (imm 8) !%rsp
  ++ ret

let print_newline =
  label "print_newline"
  ++ subq (imm 8) !%rsp
  ++ leaq (lab ".Sprint_newline") rdi
  ++ xorq !%rax !%rax ++ call "printf"
  ++ addq (imm 8) !%rsp
  ++ ret

let print_value =
  label "print_value"
  ++ movq (ind rdi) !%rbx
  ++ movq (ind ~ofs:8 rdi) !%rdi
  ++ cmpq (imm (getTypeInd Int64)) !%rbx
  ++ je ".print_value_int"
  ++ cmpq (imm (getTypeInd Bool)) !%rbx
  ++ je ".print_value_bool"
  ++ cmpq (imm (getTypeInd String)) !%rbx
  ++ je ".print_value_string" ++ label ".print_value_int" ++ call "print_int"
  ++ ret ++ label ".print_value_bool" ++ call "print_bool" ++ ret
  ++ label ".print_value_string"
  ++ call "print_string" ++ ret

(* Takes pointer in rdi *)
let checkTypeBoolInt =
  label "check_int_or_bool"
  ++ movq (ind rdi) !%rbx
  ++ movq (ind rdi) !%rcx
  ++ movq (ind rdi) !%rdx
  ++ subq (imm (getTypeInd Any)) !%rbx
  ++ subq (imm (getTypeInd Int64)) !%rcx
  ++ subq (imm (getTypeInd Bool)) !%rdx
  ++ imulq !%rcx !%rbx ++ imulq !%rdx !%rbx
  ++ cmpq (imm 0) !%rbx
  ++ jne "exit_brute" ++ ret

let exitBrute =
  label "exit_brute"
  ++ leaq (lab ".Sprint_rip") rdi
  ++ call "print_string"
  ++ movq (imm 1) !%rdi
  ++ call "exit"

let initializeVariable =
  label "init_variable"
  ++ movq (imm 8) !%rdi
  ++ call "malloc"
  ++ movq (imm 1) !%rbx
  ++ movq !%rbx (ind rax)
  ++ ret

let copy_variable (file : typedFile) typ =
  let nbChamps =
    match typ with
    | Int64 | String | Bool -> 2
    | Nothing -> 1
    | Structure s -> 1 + List.length (Smap.find s file.structures).params
    | Any -> assert false
  in
  label (".copy_variable_" ^ typ_to_string typ)
  ++ ( match typ with
     | Structure _ -> movq !%rdi !%rax
     | _ ->
         pushq !%rdi
         ++ movq (imm (nbChamps * 8)) !%rdi
         ++ call "malloc" ++ popq rdi
         ++ Array.fold_left
              (fun cur c -> cur ++ c)
              nop
              (Array.init nbChamps (fun i ->
                   movq (ind ~ofs:(8 * i) rdi) !%rbx
                   ++ movq !%rbx (ind ~ofs:(8 * i) rax))) )
  ++ ret

let compareTypes env typ =
  let labEnd = getNewLabel () in
  let labTrue = getNewLabel () in
  let labFalse = getNewLabel () in
  label ("compare_variables" ^ typ_to_string typ)
  ++ pushq !%rdi ++ pushq !%rsi
  ++ movq (ind rdi) !%rbx
  ++ cmpq (ind rsi) !%rbx
  ++ jne labFalse
  ++ ( match typ with
     | Nothing -> jmp labTrue
     | Structure s ->
         let s = Smap.find s env.structures in
         if s.isMutable then cmpq !%rdi !%rsi ++ je labTrue ++ jmp labFalse
         else
           (let rec aux paramsSeen (paramsLeft : param list) curCode =
              match paramsLeft with
              | [] -> curCode
              | p :: q ->
                  let nextCode =
                    curCode
                    ++ movq (ind rsp) !%rdi
                    ++ movq (ind ~ofs:8 rsp) !%rsi
                    ++ movq (ind ~ofs:(8 * (1 + paramsSeen)) rdi) !%rdi
                    ++ movq (ind ~ofs:(8 * (1 + paramsSeen)) rsi) !%rsi
                    ++ call "compare_variables"
                    ++ cmpq (imm 0) !%rax
                    ++ je labFalse
                  in
                  aux (1 + paramsSeen) q nextCode
            in
            aux 0 s.params nop)
           ++ jmp labTrue
     | _ ->
         movq (ind rsp) !%rcx
         ++ movq (ind ~ofs:8 rcx) !%rcx
         ++ movq (ind ~ofs:8 rsp) !%rbx
         ++ movq (ind ~ofs:8 rbx) !%rbx
         ++ cmpq !%rbx !%rcx ++ je labTrue )
  ++ label labFalse
  ++ movq (imm 0) !%rax
  ++ jmp labEnd ++ label labTrue
  ++ movq (imm 1) !%rax
  ++ label labEnd ++ popq rdi ++ popq rdi ++ ret

(* Compares rdi and rsi *)
let compareVariables types =
  label "compare_variables"
  ++ movq (ind rdi) !%rbx
  ++ List.fold_left
       (fun cur typ ->
         cur
         ++ cmpq (imm (getTypeInd typ)) !%rbx
         ++ je ("compare_variables" ^ typ_to_string typ))
       nop types
  ++ ret

let copy types =
  label "copy_variable"
  ++ movq (ind rdi) !%rbx
  ++ List.fold_left
       (fun cur typ ->
         cur
         ++ cmpq (imm (getTypeInd typ)) !%rbx
         ++ je (".copy_variable_" ^ typ_to_string typ))
       nop types
  ++ jmp "exit_brute"

(* rdi % rsi *)
let fastpow =
  label "fastpow"
  ++ movq (imm 1) !%rax
  ++ label ".fastpow1"
  ++ cmpq (imm 0) !%rsi
  ++ je ".fastpowend" ++ movq !%rsi !%rbx
  ++ andq (imm 1) !%rbx
  ++ testq !%rbx !%rbx ++ je ".fastpow2" ++ imulq !%rdi !%rax
  ++ label ".fastpow2" ++ imulq !%rdi !%rdi
  ++ sarq (imm 1) !%rsi
  ++ jmp ".fastpow1" ++ label ".fastpowend" ++ ret

let default_data =
  label ".Sprint_int" ++ string "%lld"
  ++ (label ".Sprint_string" ++ string "%s")
  ++ (label ".Sprint_true" ++ string "true")
  ++ (label ".Sprint_false" ++ string "false")
  ++ (label ".Sprint_newline" ++ string "\n")
  ++ (label ".Sprint_rip" ++ string "RIP\n")
  ++ (label ".Sprint_not_rip" ++ string "NOT RIP\n")
