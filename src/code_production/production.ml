open Ast
open X86_64
open Production_util

let frame_size = ref 0

let rec compileExpr (typedExpr : typedExpr) (globalEnv : environment)
    (localEnv : localEnv) next =
  match typedExpr.desc with
  | Tcst (Cint x) ->
      allocateBlock 2
      ++ movq (imm (getTypeInd Int64)) !%rbx
      ++ movq !%rbx (ind rax)
      ++ movq (imm64 x) !%rbx
      ++ movq !%rbx (ind ~ofs:8 rax)
  | Tcst (Cstring s) ->
      allocateBlock 2
      ++ movq (imm (getTypeInd String)) !%rbx
      ++ movq !%rbx (ind rax)
      ++ leaq (lab (getStringLabel s)) rbx
      ++ movq !%rbx (ind ~ofs:8 rax)
  | Tcst (Cbool b) ->
      allocateBlock 2
      ++ movq (imm (getTypeInd Bool)) !%rbx
      ++ movq !%rbx (ind rax)
      ++ movq (imm (Bool.to_int b)) !%rbx
      ++ movq !%rbx (ind ~ofs:8 rax)
  | Tident id -> getVariable localEnv id
  | Tlvalue (TLvalueIdent (_, id)) -> getVariable localEnv id
  | Tassign (TLvalueIdent (typ, id), t) -> (
      compileExpr t globalEnv localEnv next
      ++ ( if typ <> Any then
           pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible typ
           ++ popq rax
         else nop )
      ++
      try
        let pos = Smap.find id localEnv in
        movq !%rax (ind ~ofs:pos rbp)
      with Not_found -> movq !%rax (lab (getGlobalVariable id)) )
  | Tcall ("print", l) ->
      List.fold_left
        (fun cur e ->
          cur
          ++ compileExpr e globalEnv localEnv next
          ++ movq !%rax !%rdi ++ call "print_value")
        nop l
  | Tcall ("println", l) ->
      compileExpr
        { typedExpr with desc = Tcall ("print", l) }
        globalEnv localEnv next
      ++ call "print_newline"
  | Tunop (Uneg, t) ->
      compileExpr t globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ negq !%rdi ++ call "load_int"
  | Tbinop (t1, op, t2) when op = Badd || op = Bsub || op = Bmul || op = Bmod ->
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ movq (ind ~ofs:8 rbx) !%rcx
      ++ popq rdi
      ++ ( match op with
         | Badd -> addq !%rcx !%rdi
         | Bsub -> subq !%rcx !%rdi
         | Bmul -> imulq !%rcx !%rdi
         | Bmod ->
             movq !%rdi !%rax ++ cqto
             ++ cmpq (imm 0) !%rcx
             ++ je "exit_brute" ++ idivq !%rcx ++ movq !%rdx !%rdi
         | _ -> assert false )
      ++ call "load_int"
  | Tbinop (t1, Bpow, t2) ->
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ movq (ind ~ofs:8 rbx) !%rsi
      ++ popq rdi ++ call "fastpow" ++ movq !%rax !%rdi ++ call "load_int"
  | Tbinop (t1, Band, t2) ->
      let endLabel = getNewLabel () in
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ cmpq (imm 0) !%rdi
      ++ je endLabel
      ++ compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ label endLabel ++ call "load_bool"
  | Tbinop (t1, op, t2) when op = Blt || op = Ble || op = Bgt || op = Bge ->
      let labName = getNewLabel () in
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ call "check_int_or_bool" ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ call "check_int_or_bool" ++ popq rbx
      ++ movq (ind ~ofs:8 rbx) !%rbx
      ++ popq rcx
      ++ movq (imm 0) !%rdi
      ++ cmpq !%rcx !%rbx (* rft - lgt *)
      ++ ( match op with
         | Blt -> jle
         | Ble -> jl
         | Bgt -> jge
         | Bge -> jg
         | _ -> assert false )
           labName
      ++ movq (imm 1) !%rdi
      ++ label labName ++ call "load_bool"
  | Tbinop (t1, op, t2) when op = Beq || op = Bdiff ->
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax
      ++ compileExpr t2 globalEnv localEnv next
      ++ movq !%rax !%rdi ++ popq rsi ++ call "compare_variables"
      ++ (if op = Bdiff then negq !%rax ++ incq !%rax else nop)
      ++ movq !%rax !%rdi ++ call "load_bool"
  | Tbinop (t1, Bor, t2) ->
      let endLabel = getNewLabel () in
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ cmpq (imm 0) !%rdi
      ++ jne endLabel
      ++ compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ label endLabel ++ call "load_bool"
  | Tunop (Unot, t1) ->
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ xorq (imm 1) !%rdi
      ++ call "load_bool"
  | Tcall ("div", l) ->
      let t1, t2 = match l with [ a; b ] -> (a, b) | _ -> assert false in
      compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rcx
      ++ movq (ind ~ofs:8 rcx) !%rcx
      ++ popq rax ++ cqto
      ++ cmpq (imm 0) !%rcx
      ++ je "exit_brute" ++ idivq !%rcx ++ movq !%rax !%rdi ++ call "load_int"
  | Tentier_ident (x, id) ->
      getVariable localEnv id ++ pushq !%rax ++ movq !%rax !%rdi
      ++ checkTypeCompatible Int64 ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ imulq (imm64 x) !%rdi
      ++ call "load_int"
  | Tentier_parg_bloc (x, l) ->
      List.fold_left
        (fun cur t -> cur ++ compileExpr t globalEnv localEnv next)
        nop l
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rdi
      ++ movq (ind ~ofs:8 rdi) !%rdi
      ++ imulq (imm64 x) !%rdi
      ++ call "load_int"
  | Tbloc l ->
      List.fold_left
        (fun cur t -> cur ++ compileExpr t globalEnv localEnv next)
        nop l
  | Tpard_ident (t, id) ->
      compileExpr t globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ getVariable localEnv id ++ pushq !%rax ++ movq !%rax !%rdi
      ++ checkTypeCompatible Int64 ++ popq rbx
      ++ movq (ind ~ofs:8 rbx) !%rbx
      ++ popq rdi ++ imulq !%rbx !%rdi ++ call "load_int"
  | Tif (cond, b1, b2) ->
      let labFalse = getNewLabel () in
      let labEnd = getNewLabel () in
      compileExpr cond globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rbx
      ++ movq (ind ~ofs:8 rbx) !%rbx
      ++ cmpq (imm 0) !%rbx
      ++ je labFalse
      ++ List.fold_left
           (fun cur t -> cur ++ compileExpr t globalEnv localEnv next)
           nop b1
      ++ jmp labEnd ++ label labFalse
      ++ List.fold_left
           (fun cur t -> cur ++ compileExpr t globalEnv localEnv next)
           nop b2
      ++ label labEnd
  | Twhile (cond, b, inside) ->
      let labCond = getNewLabel () in
      let labEnd = getNewLabel () in
      let newLocalVars =
        Smap.fold (fun id _ cur -> Smap.remove id cur) localEnv inside.variables
      in
      let newLocalEnv =
        let pos = ref next in
        Smap.fold
          (fun id typ cur ->
            pos := !pos + 8;
            Smap.add id (8 - !pos) cur)
          newLocalVars localEnv
      in
      let initCode =
        Smap.fold
          (fun id _ cur ->
            cur ++ call "init_variable"
            ++ movq !%rax (ind ~ofs:(Smap.find id newLocalEnv) rbp))
          newLocalVars nop
      in

      label labCond
      ++ compileExpr cond globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Bool ++ popq rbx
      ++ movq (ind ~ofs:8 rbx) !%rbx
      ++ cmpq (imm 0) !%rbx
      ++ je labEnd
      ++ initCode
      ++ ( frame_size :=
             max !frame_size (next + (8 * Smap.cardinal newLocalVars));
           List.fold_left
             (fun cur t ->
               cur
               ++ compileExpr t globalEnv newLocalEnv
                    (next + (8 * Smap.cardinal newLocalVars)))
             nop b )
      ++ jmp labCond ++ label labEnd
      ++ movq (lab (getGlobalVariable "nothing")) !%rax
  | Tcall (id, l) ->
      List.fold_left
        (fun cur t ->
          compileExpr t globalEnv localEnv next ++ pushq !%rax ++ cur)
        nop l
      ++ call (".D" ^ id)
  | Treturn (None, s) ->
      movq (lab (getGlobalVariable "nothing")) !%rax ++ jmp (".E" ^ s)
  | Treturn (Some t, s) ->
      compileExpr t globalEnv localEnv next ++ jmp (".E" ^ s)
  | Tfor (id, start, last, b, inside) ->
      let checkCondLab = getNewLabel () in
      let bodyLab = getNewLabel () in
      let newLocalVars =
        Smap.fold (fun id _ cur -> Smap.remove id cur) localEnv inside.variables
      in
      let newLocalVars = Smap.add id Int64 newLocalVars in
      let newLocalEnv =
        let pos = ref next in
        Smap.fold
          (fun id _ cur ->
            pos := !pos + 8;
            Smap.add id (8 - !pos) cur)
          newLocalVars localEnv
      in
      let initCode =
        Smap.fold
          (fun id _ cur ->
            cur ++ call "init_variable"
            ++ movq !%rax (ind ~ofs:(Smap.find id newLocalEnv) rbp))
          newLocalVars nop
      in

      compileExpr start globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ compileExpr last globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi ++ checkTypeCompatible Int64
      ++ popq rbx
      ++ pushq (ind ~ofs:8 rbx)
      ++ popq rbx ++ popq rcx ++ pushq !%rbx ++ pushq !%rcx ++ initCode ++ jmp checkCondLab
      ++ label bodyLab ++ popq rbx ++ movq !%rbx !%rdi ++ pushq !%rbx
      ++ call "load_int"
      ++ movq !%rax (ind ~ofs:(Smap.find id newLocalEnv) rbp)
      ++ ( frame_size :=
             max !frame_size (next + (8 * Smap.cardinal newLocalVars));
           List.fold_left
             (fun cur e ->
               cur
               ++ compileExpr e globalEnv newLocalEnv
                    (next + (8 * Smap.cardinal newLocalVars)))
             nop b )
      ++ popq rcx ++ incq !%rcx ++ pushq !%rcx ++ label checkCondLab ++ popq rcx
      ++ popq rbx ++ pushq !%rbx ++ pushq !%rcx ++ cmpq !%rbx !%rcx
      ++ jle bodyLab ++ popq rcx ++ popq rcx ++ call "init_variable"
      ++ movq !%rax (ind ~ofs:(Smap.find id newLocalEnv) rbp)
      ++ movq (lab (getGlobalVariable "nothing")) !%rax
  | Tstructure (s, l) ->
      let rec aux curCode paramsPicked (paramsLeft : param list) exprsLeft =
        match exprsLeft with
        | [] -> curCode
        | t :: q ->
            let p = List.hd paramsLeft in
            let nextCode =
              curCode
              ++ compileExpr t globalEnv localEnv next
              ++ ( if snd p.desc <> Any then
                   pushq !%rax ++ movq !%rax !%rdi
                   ++ checkTypeCompatible (snd p.desc)
                   ++ popq rax
                 else nop )
              ++ movq (ind rsp) !%r10
              ++ movq !%rax (ind ~ofs:(8 * (paramsPicked + 1)) r10)
            in
            aux nextCode (paramsPicked + 1) (List.tl paramsLeft) q
      in
      movq (imm (8 * (1 + List.length s.params))) !%rdi
      ++ call "malloc" ++ pushq !%rax
      ++ movq (imm (getTypeInd (Structure s.name))) !%rbx
      ++ movq !%rbx (ind rax)
      ++ aux nop 0 s.params l ++ popq rax
  | Tlvalue (TLvalueStruct (_, t, id)) ->
      let s = Smap.find id globalEnv.fieldNames in
      let rec getInfo paramsSeen (paramsLeft : param list) =
        match paramsLeft with
        | p :: q when fst p.desc = id -> (paramsSeen, snd p.desc)
        | _ :: q -> getInfo (paramsSeen + 1) q
        | [] -> assert false
      in
      let info = getInfo 0 s.params in
      compileExpr t globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi
      ++ checkTypeCompatible (Structure s.name)
      ++ popq rax
      ++ movq (ind ~ofs:(8 * (fst info + 1)) rax) !%rax
  | Tassign (TLvalueStruct (typ, t1, id), t2) ->
      let s = Smap.find id globalEnv.fieldNames in
      let rec getInfo paramsSeen (paramsLeft : param list) =
        match paramsLeft with
        | p :: q when fst p.desc = id -> (paramsSeen, snd p.desc)
        | _ :: q -> getInfo (paramsSeen + 1) q
        | [] -> assert false
      in
      let info = getInfo 0 s.params in
      compileExpr t2 globalEnv localEnv next
      ++ pushq !%rax
      ++ ( if snd info <> Any then
           movq !%rax !%rdi ++ checkTypeCompatible (snd info)
         else nop )
      ++ compileExpr t1 globalEnv localEnv next
      ++ pushq !%rax ++ movq !%rax !%rdi
      ++ checkTypeCompatible (Structure s.name)
      ++ popq rbx ++ popq rax
      ++ movq !%rax (ind ~ofs:(8 * (1 + fst info)) rbx)
  | _ -> assert false

let writeDispatch file fName types =
  let functions = Smap.find fName file.functions in
  let code = ref nop in
  let labExit = getNewLabel () in

  let rec aux functionsLeft paramsPicked paramsToPick =
    if paramsToPick = 0 then
      match functionsLeft with
      | [ f ] -> code := !code ++ call (".F" ^ f.name) ++ jmp labExit
      | [] -> code := jmp "exit_brute"
      | _ -> (
          try
            let f = findMinFunction functionsLeft in
            code := !code ++ call (".F" ^ f.name) ++ jmp labExit;
          with Not_found -> code := !code ++ jmp "exit_brute" )
    else if
      List.for_all
        (fun f -> snd (List.nth f.params paramsPicked).desc = Any)
        functionsLeft
    then aux functionsLeft (paramsPicked + 1) (paramsToPick - 1)
    else
      List.iter
        (fun typ ->
          if typ <> Any then
            let filteredFunctions =
              List.filter
                (fun f ->
                  match snd (List.nth f.params paramsPicked).desc with
                  | t when t = Any || t = typ -> true
                  | _ -> false)
                functionsLeft
            in
            if filteredFunctions <> [] then (
              let labelExit = getNewLabel () in
              let filter_code =
                movq (ind ~ofs:(8 * (2 + paramsPicked)) rbp) !%rbx
                ++ movq (ind rbx) !%rbx
                ++ cmpq (imm (getTypeInd typ)) !%rbx
                ++ jne labelExit
              in
              code := !code ++ filter_code;
              aux filteredFunctions (1 + paramsPicked) (paramsToPick - 1);
              code := !code ++ label labelExit ))
        types
  in
  aux functions 0 (List.length (List.hd functions).params);

  label (".D" ^ fName)
  ++ pushq !%rbp
  ++ leaq (ind rsp) rbp
  ++ !code ++ jmp "exit_brute" ++ label labExit
  ++ List.fold_left
       (fun cur _ -> cur ++ popq rbx)
       (popq rbp ++ popq rcx)
       (List.hd functions).params
  ++ pushq !%rcx ++ ret

let compileFunction (f : typedFunction) file =
  let localEnv =
    Smap.fold
      (fun id _ curEnv -> Smap.add id (-(Smap.cardinal curEnv + 2) * 8) curEnv)
      f.localEnv Smap.empty
  in
  let codeInit =
    Smap.fold
      (fun id pos cur ->
        cur ++ call "init_variable" ++ movq !%rax (ind ~ofs:pos rbp))
      localEnv nop
  in

  let localEnv =
    let curPos = ref 1 in
    List.fold_left
      (fun cur (p : param) ->
        incr curPos;
        Smap.add (fst p.desc) (8 * !curPos) cur)
      localEnv f.params
  in

  frame_size := 8 * (Smap.cardinal f.localEnv + 2);
  let code =
    List.fold_left
      (fun cur t ->
        cur
        ++ compileExpr t file.globalEnv localEnv
             (8 * (Smap.cardinal f.localEnv + 2)))
      nop f.bloc
  in
  label (".F" ^ f.name)
  ++ subq (imm (!frame_size - 16)) !%rsp
  ++ codeInit ++ code
  ++ label (".E" ^ f.name)
  ++ ( match f.retType with
     | Any -> nop
     | _ ->
         pushq !%rax ++ movq !%rax !%rdi
         ++ checkTypeCompatible f.retType
         ++ popq rax )
  ++ addq (imm (!frame_size - 16)) !%rsp
  ++ ret

let compile (file : typedFile) =
  Hashtbl.add typeToInt Nothing 0;
  Hashtbl.add intToType 0 Nothing;
  ignore (getGlobalVariable "nothing");
  Smap.iter (fun s _ -> ignore (getTypeInd (Structure s))) file.structures;
  let types = listOfTypes file in

  let codeDispatches =
    Smap.fold
      (fun fName _ cur -> cur ++ writeDispatch file fName types)
      file.functions nop
  in

  let codeFunctions =
    Smap.fold
      (fun fName l cur ->
        cur ++ List.fold_left (fun cur f -> cur ++ compileFunction f file) nop l)
      file.functions nop
  in

  frame_size := 0;

  let codeExprs =
    List.fold_left
      (fun cur e -> cur ++ compileExpr e file.globalEnv Smap.empty 0)
      nop file.exprs
  in

  let codeInitGlobal =
    Hashtbl.fold
      (fun id l cur ->
        cur ++ call "init_variable"
        ++ (match id with "nothing" -> movq (imm 0) (ind rax) | _ -> nop)
        ++ movq !%rax (lab l))
      globalVariables nop
  in

  let text =
    globl "main" ++ label "main" ++ codeInitGlobal
    ++ subq (imm !frame_size) !%rsp
    ++ leaq (ind ~ofs:(!frame_size - 8) rsp) rbp
    ++ codeExprs ++ label "endExprs"
    ++ addq (imm !frame_size) !%rsp
    ++ xorq !%rax !%rax ++ ret ++ codeDispatches ++ codeFunctions
    ++ print_newline ++ print_string ++ print_bool ++ print_int ++ exitBrute
    ++ print_value ++ load_int ++ load_bool ++ checkTypeBoolInt ++ fastpow
    ++ initializeVariable
    ++ List.fold_left
         (fun cur typ -> cur ++ copy_variable file typ)
         nop (List.tl types)
    ++ copy (List.tl types)
    ++ compareVariables (List.tl types)
    ++ List.fold_left
         (fun cur typ -> cur ++ compareTypes file typ)
         nop (List.tl types)
  in
  {
    data =
      default_data
      ++ Hashtbl.fold
           (fun id lab cur -> cur ++ label lab ++ dquad [ 0 ])
           globalVariables nop
      ++ Hashtbl.fold
           (fun id lab cur -> cur ++ label lab ++ string id)
           strings nop;
    text;
  }
