open Ast
open Typing_util

let addDecl (curEnv : environment) = function
  | Dstruct s ->
      if Smap.mem s.name curEnv.structures then
        raise
          (TypingError (s.loc, "Structure name " ^ s.name ^ " already exists."));
      if Smap.mem s.name curEnv.functions then
        raise
          (TypingError
             ( s.loc,
               "Structure name " ^ s.name ^ " already exists as a function." ));
      let fields = List.map (fun (x : param) -> x.desc) s.params in

      List.fold_left
        (fun curEnv ss ->
          if Smap.mem (fst ss) curEnv.fieldNames then
            raise
              (TypingError (s.loc, "Field name " ^ fst ss ^ " already exists !"));
          if not (isWellFormed curEnv (snd ss)) then
            raise
              (TypingError
                 (s.loc, "type of field " ^ fst ss ^ " not well formed !"));
          { curEnv with fieldNames = Smap.add (fst ss) s curEnv.fieldNames })
        { curEnv with structures = Smap.add s.name s curEnv.structures }
        fields
  | Dfun fonc ->
      let s = fonc.name in
      if Smap.mem s curEnv.structures then
        raise
          (TypingError
             (fonc.loc, "Function name " ^ s ^ " already exists as a structure."));
      if s = "div" || s = "print" || s = "println" then
        raise (TypingError (fonc.loc, s ^ " is a reserved function name."));
      if not (isWellFormed curEnv fonc.retType) then
        raise (TypingError (fonc.loc, s ^ " return type is not well typed"));
      let _ =
        List.fold_left
          (fun curNames (id, t) ->
            if not (isWellFormed curEnv t) then
              raise (TypingError (fonc.loc, s ^ " type is not well formed."));
            if Sset.mem id curNames then
              raise
                (TypingError
                   (fonc.loc, s ^ " appears multiple times as an argument"));
            Sset.add id curNames)
          Sset.empty
          (List.map (fun (x : param) -> x.desc) fonc.params)
      in
      let s = fonc.name ^ string_of_int (List.length fonc.params) in
      let curFuns = try Smap.find s curEnv.functions with Not_found -> [] in
      if List.exists (fun f -> equalFunctions f fonc) curFuns then
        raise
          (TypingError
             (fonc.loc, "Another function with same name accepts same arguments"));
      {
        structures = curEnv.structures;
        fieldNames = curEnv.fieldNames;
        functions =
          Smap.add s ({ fonc with name = s } :: curFuns) curEnv.functions;
        variables = curEnv.variables;
      }
  | Dexpr expr -> addAssigns false false curEnv expr

let fileToEnv =
  List.fold_left (fun curEnv decl -> addDecl curEnv decl) emptyEnvironment

let rec typeExpr env (expr : expr) =
  match expr.desc with
  | Ecst c ->
      {
        desc = Tcst c;
        loc = expr.loc;
        typ =
          ( match c with
          | Cint _ -> Int64
          | Cbool _ -> Bool
          | Cstring _ -> String );
      }
  | Eident id ->
      {
        desc = Tident id;
        loc = expr.loc;
        typ =
          ( try Smap.find id env.variables
            with Not_found ->
              raise (TypingError (expr.loc, "Undefined variable " ^ id ^ "."))
          );
      }
  | Ebinop (e1, binop, e2) when binop = Beq || binop = Bdiff ->
      {
        desc = Tbinop (typeExpr env e1, binop, typeExpr env e2);
        loc = expr.loc;
        typ = Bool;
      }
  | Ebinop (e1, binop, e2)
    when binop = Badd || binop = Bsub || binop = Bmul || binop = Bmod
         || binop = Bpow ->
      let e1 = typeExpr env e1 in
      let e2 = typeExpr env e2 in
      if not (compatibleTypes e1.typ Int64) then
        raise (TypingError (e1.loc, "Expected integer"));
      if not (compatibleTypes e2.typ Int64) then
        raise (TypingError (e2.loc, "Expected integer"));
      { desc = Tbinop (e1, binop, e2); loc = expr.loc; typ = Int64 }
  | Ebinop (e1, binop, e2) when binop = Band || binop = Bor ->
      let e1 = typeExpr env e1 in
      let e2 = typeExpr env e2 in
      if not (compatibleTypes e1.typ Bool) then
        raise (TypingError (e1.loc, "Expected boolean"));
      if not (compatibleTypes e2.typ Bool) then
        raise (TypingError (e2.loc, "Expected boolean"));
      { desc = Tbinop (e1, binop, e2); loc = expr.loc; typ = Bool }
  | Ebinop (e1, binop, e2) ->
      let e1 = typeExpr env e1 in
      let e2 = typeExpr env e2 in
      if not (compatibleTypes e1.typ Bool || compatibleTypes e1.typ Int64) then
        raise (TypingError (e1.loc, "Expected boolean or integer"));
      if not (compatibleTypes e2.typ Bool || compatibleTypes e2.typ Int64) then
        raise (TypingError (e2.loc, "Expected boolean or integer"));
      { desc = Tbinop (e1, binop, e2); loc = expr.loc; typ = Bool }
  | Eunop (Unot, e) ->
      let e = typeExpr env e in
      if not (compatibleTypes e.typ Bool) then
        raise (TypingError (e.loc, "Expected boolean"));
      { desc = Tunop (Unot, e); loc = expr.loc; typ = Bool }
  | Eunop (Uneg, e) ->
      let e = typeExpr env e in
      if not (compatibleTypes e.typ Int64) then
        raise (TypingError (e.loc, "Expected integer"));
      { desc = Tunop (Uneg, e); loc = expr.loc; typ = Int64 }
  | Ebloc l ->
      let l = List.map (typeExpr env) l in
      {
        desc = Tbloc l;
        loc = expr.loc;
        typ = List.fold_left (fun cur e -> e.typ) Any l;
      }
  | Eentier_ident (x, id) ->
      { desc = Tentier_ident (x, id); typ = Int64; loc = expr.loc }
  | Epard_ident (e, id) ->
      let e = typeExpr env e in
      if not (compatibleTypes e.typ Int64) then
        raise (TypingError (e.loc, "Expected an integer"));
      { desc = Tpard_ident (e, id); loc = expr.loc; typ = Int64 }
  | Eentier_parg_bloc (x, b) ->
      let l = List.map (typeExpr env) b in
      let lastTyp = List.fold_left (fun cur e -> e.typ) Any l in
      if not (compatibleTypes lastTyp Int64) then
        raise
          (TypingError
             ( (List.fold_left (fun cur e -> e) (List.hd l) l).loc,
               "Expected an integer !" ));
      { desc = Tentier_parg_bloc (x, l); loc = expr.loc; typ = Int64 }
  | Ecall (id, l) when id = "div" ->
      let x, y =
        match l with
        | [ a; b ] -> (a, b)
        | _ ->
            raise
              (TypingError (expr.loc, "Expected two arguments for division"))
      in
      let x = typeExpr env x in
      let y = typeExpr env y in
      if not (compatibleTypes x.typ Int64) then
        raise (TypingError (x.loc, "Expected an integer"));
      if not (compatibleTypes y.typ Int64) then
        raise (TypingError (y.loc, "Expected an integer"));
      { desc = Tcall (id, [ x; y ]); typ = Int64; loc = expr.loc }
  | Ecall (id, l) when id = "print" || id = "println" ->
      let l = List.map (typeExpr env) l in
      { desc = Tcall (id, l); loc = expr.loc; typ = Nothing }
  | Ecall (id, l) when Smap.mem id env.structures ->
      {
        desc =
          Tstructure (Smap.find id env.structures, List.map (typeExpr env) l);
        loc = expr.loc;
        typ = Structure id;
      }
  | Ecall (id, l) -> (
      let id = id ^ string_of_int (List.length l) in
      let compatibleFunc =
        try Smap.find id env.functions with Not_found -> []
      in
      let l = List.map (typeExpr env) l in
      let compatibleFunc =
        let rec isCompatible l1 (l2 : param list) =
          match (l1, l2) with
          | [], [] -> true
          | a :: p, b :: q when compatibleTypes a.typ (snd b.desc) ->
              isCompatible p q
          | _ -> false
        in
        List.filter
          (fun (p : fonction) -> isCompatible l p.params)
          compatibleFunc
      in
      let compatibleFunc =
        List.fold_left
          (fun curComp f ->
            if biggerParams l f then
              if
                List.exists
                  (fun f' -> biggerParams l f' && smallerFunction f' f)
                  curComp
              then curComp
              else
                f :: List.filter (fun f' -> not (smallerFunction f f')) curComp
            else f :: curComp)
          [] compatibleFunc
      in
      let biggerCompatible =
        List.filter (fun f -> biggerParams l f) compatibleFunc
      in
      if List.length biggerCompatible >= 2 then
        raise
          (TypingError (expr.loc, "Multiple compatible functions : ambiguity."));
      let nbCompatibles = List.length compatibleFunc in
      match nbCompatibles with
      | 0 ->
          raise (TypingError (expr.loc, "No compatible function or structure."))
      | 1 ->
          {
            desc = Tcall (id, l);
            typ = (List.hd compatibleFunc).retType;
            loc = expr.loc;
          }
      | _ -> { desc = Tcall (id, l); typ = Any; loc = expr.loc } )
  | Ereturn None -> { desc = Treturn (None, ""); typ = Any; loc = expr.loc }
  | Ereturn (Some e) ->
      let e = typeExpr env e in
      { desc = Treturn (Some e, ""); typ = Any; loc = expr.loc }
  | Efor (varName, start, last, b) ->
      let start = typeExpr env start in
      if not (compatibleTypes Int64 start.typ) then
        raise (TypingError (start.loc, "Expected integer"));
      let last = typeExpr env last in
      if not (compatibleTypes Int64 last.typ) then
        raise (TypingError (last.loc, "Expected integer"));
      let newEnv =
        { env with variables = Smap.add varName Int64 env.variables }
      in
      let newEnv = List.fold_left (addAssigns false true) newEnv b in
      let insideEnv = {env with variables = Smap.empty} in
      let insideEnv = {insideEnv with variables = Smap.add varName Int64 insideEnv.variables} in
      let insideEnv = List.fold_left (addAssigns false true) insideEnv b in
      let l = List.map (typeExpr newEnv) b in
      { desc = Tfor (varName, start, last, l, insideEnv); loc = expr.loc; typ = Nothing }
  | Ewhile (cond, b) ->
      let cond = typeExpr env cond in
      if not (compatibleTypes Bool cond.typ) then
        raise (TypingError (cond.loc, "Expected boolean"));
      let newEnv = List.fold_left (addAssigns false true) env b in
      let insideEnv = List.fold_left (addAssigns false true) {env with variables = Smap.empty} b in
      let l = List.map (typeExpr newEnv) b in
      { desc = Twhile (cond, l, insideEnv); loc = expr.loc; typ = Nothing }
  | Eif (cond, b1, b2) ->
      let cond = typeExpr env cond in
      if not (compatibleTypes cond.typ Bool) then
        raise (TypingError (cond.loc, "Expected boolean"));
      let l1 = List.map (typeExpr env) b1 in
      let l2 = List.map (typeExpr env) b2 in
      let t1 = List.fold_left (fun cur x -> x.typ) Nothing l1 in
      let t2 = List.fold_left (fun cur x -> x.typ) Nothing l2 in
      {
        desc = Tif (cond, l1, l2);
        loc = expr.loc;
        typ = (if t1 = t2 then t1 else Any);
      }
  | Eassign (LvalueIdent id, e) ->
      let e = typeExpr env e in
      let t1 =
        try Smap.find id env.variables
        with Not_found ->
          raise (TypingError (e.loc, "undefined variable " ^ id ^ "."))
      in
      if t1 <> Any && t1 <> e.typ then
        raise (TypingError (expr.loc, "Both sides should have same type !"));
      { desc = Tassign (TLvalueIdent (t1, id), e); typ = e.typ; loc = expr.loc }
  | Eassign (LvalueStruct (e1, id), e2) ->
      let s =
        try Smap.find id env.fieldNames
        with Not_found ->
          raise
            (TypingError (expr.loc, "No structure with field name " ^ id ^ "."))
      in
      let e1 = typeExpr env e1 in
      if not (compatibleTypes (Structure s.name) e1.typ) then
        raise (TypingError (e1.loc, "Expected structure " ^ s.name ^ "."));
      if not s.isMutable then
        raise
          (TypingError (expr.loc, "Structure " ^ s.name ^ " is not mutable !"));
      let typ = List.assoc id (List.map (fun (p : param) -> p.desc) s.params) in
      let e2 = typeExpr env e2 in
      if not (compatibleTypes typ e2.typ) then
        raise (TypingError (e2.loc, "Expected type " ^ typ_to_string typ ^ "."));
      { desc = Tassign (TLvalueStruct (typ, e1, id), e2); loc = expr.loc; typ }
  | Elvalue (LvalueIdent id) ->
      let typ =
        try Smap.find id env.variables
        with Not_found ->
          raise (TypingError (expr.loc, "undefined variable " ^ id ^ "."))
      in
      { desc = Tlvalue (TLvalueIdent (typ, id)); loc = expr.loc; typ }
  | Elvalue (LvalueStruct (e, id)) ->
      let s =
        try Smap.find id env.fieldNames
        with Not_found ->
          raise
            (TypingError (expr.loc, "No structure with field name " ^ id ^ "."))
      in
      let e = typeExpr env e in
      if not (compatibleTypes e.typ (Structure s.name)) then
        raise (TypingError (e.loc, "Expected structure " ^ s.name ^ "."));
      let typ = List.assoc id (List.map (fun (p : param) -> p.desc) s.params) in
      { desc = Tlvalue (TLvalueStruct (typ, e, id)); loc = expr.loc; typ }

let rec wrongReturn typ e =
  match e.desc with
  | Tbinop (e1, _, e2) | Tassign (TLvalueStruct (_, e1, _), e2) ->
      wrongReturn typ e1;
      wrongReturn typ e2
  | Tunop (_, e) | Tpard_ident (e, _) | Tlvalue (TLvalueStruct (_, e, _)) ->
      wrongReturn typ e
  | Tbloc b | Tentier_parg_bloc (_, b) | Tcall (_, b) ->
      List.iter (wrongReturn typ) b
  | Treturn (None, "") ->
      if not (compatibleTypes typ Nothing) then
        raise
          (TypingError
             (e.loc, "Expected return of type " ^ typ_to_string typ ^ "."))
  | Treturn (Some f, _) ->
      if not (compatibleTypes f.typ typ) then
        raise
          (TypingError
             (e.loc, "Expected return of type " ^ typ_to_string typ ^ "."))
  | Tfor (_, e1, e2, b, _) ->
      wrongReturn typ e1;
      wrongReturn typ e2;
      List.iter (wrongReturn typ) b
  | Twhile (e, b, _) ->
      wrongReturn typ e;
      List.iter (wrongReturn typ) b
  | Tif (e, b1, b2) ->
      wrongReturn typ e;
      List.iter (wrongReturn typ) b1;
      List.iter (wrongReturn typ) b2
  | _ -> ()

let typeFunction env (fonction : fonction) =
  let newVariables =
    List.fold_left
      (fun cur (p : param) -> Smap.add (fst p.desc) (snd p.desc) cur)
      Smap.empty fonction.params
  in
  let localEnv =
    List.fold_left
      (fun curEnv expr -> addAssigns true true curEnv expr)
      { emptyEnvironment with variables = newVariables }
      fonction.bloc
  in

  let localEnv =
    List.fold_left
      (fun curEnv (p : param) -> Smap.remove (fst p.desc) curEnv)
      localEnv.variables fonction.params
  in

  let env =
    List.fold_left
      (fun curEnv expr -> addAssigns false true curEnv expr)
      {
        env with
        variables =
          Smap.fold
            (fun id typ cur -> Smap.add id typ cur)
            newVariables env.variables;
      }
      fonction.bloc
  in

  let bloc = List.map (typeExpr env) fonction.bloc in
  let lastTyp = List.fold_left (fun cur x -> x.typ) Nothing bloc in
  if not (compatibleTypes lastTyp fonction.retType) then
    raise
      (TypingError
         ( fonction.loc,
           "Function does not return type "
           ^ typ_to_string fonction.retType
           ^ "." ));
  List.iter (wrongReturn fonction.retType) bloc;
  {
    name = fonction.name;
    params = fonction.params;
    retType = fonction.retType;
    bloc;
    loc = fonction.loc;
    localEnv;
  }

let typeDecl file = function
  | Dstruct s -> { file with structures = Smap.add s.name s file.structures }
  | Dexpr e ->
      {
        file with
        exprs = modReturns "" (typeExpr file.globalEnv e) :: file.exprs;
      }
  | Dfun fonc -> file

let addFunctions file =
  let aux file (fonc : fonction) =
    let oldFuns =
      try Smap.find fonc.name file.functions with Not_found -> []
    in
    let fonc = typeFunction file.globalEnv fonc in
    let name = fonc.name ^ "_" ^ string_of_int (List.length oldFuns) in
    {
      file with
      functions =
        Smap.add fonc.name
          ( { fonc with name; bloc = List.map (modReturns name) fonc.bloc }
          :: oldFuns )
          file.functions;
    }
  in
  Smap.fold
    (fun _ l cur -> List.fold_left aux cur l)
    file.globalEnv.functions file

let typeFile f =
  let emptyFile =
    {
      globalEnv =
        List.fold_left
          (fun cur e -> addAssigns true false cur e)
          (fileToEnv f)
          (List.filter_map
             (fun d -> match d with Dexpr e -> Some e | _ -> None)
             f);
      exprs = [];
      functions = Smap.empty;
      structures = Smap.empty;
    }
  in
  let file = List.fold_left typeDecl emptyFile f in
  let file = addFunctions file in
  { file with exprs = List.rev file.exprs }
