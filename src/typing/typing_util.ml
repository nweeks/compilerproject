open Ast

exception TypingError of loc * string

let emptyEnvironment =
  {
    structures = Smap.empty;
    fieldNames = Smap.empty;
    functions = Smap.empty;
    variables = Smap.add "nothing" Nothing Smap.empty;
  }

let compatibleTypes t1 t2 =
  match (t1, t2) with Any, _ | _, Any -> true | _ -> t1 = t2

let smallerType t1 t2 = match (t1, t2) with _, Any -> true | _, _ -> t1 = t2

let isWellFormed (curEnv : environment) = function
  | Structure id -> Smap.mem id curEnv.structures
  | _ -> true

let equalFunctions (f1 : fonction) (f2 : fonction) =
  let rec aux (l1 : param list) (l2 : param list) =
    match (l1, l2) with
    | [], [] -> true
    | a :: p, b :: q -> snd a.desc = snd b.desc && aux p q
    | _ -> false
  in
  aux f1.params f2.params

let smallerFunction (f1 : fonction) (f2 : fonction) =
  let rec aux (l1 : param list) (l2 : param list) =
    match (l1, l2) with
    | [], [] -> true
    | a :: p, b :: q -> smallerType (snd a.desc) (snd b.desc) && aux p q
    | _ -> false
  in
  aux f1.params f2.params

let biggerParams (p : typedExpr list) (f : fonction) =
  let rec aux (l1 : typedExpr list) (l2 : param list) =
    match (l1, l2) with
    | [], [] -> true
    | a :: p, b :: q -> smallerType a.typ (snd b.desc) && aux p q
    | _ -> false
  in
  aux p f.params

let addAssigns recurse takeNothing curEnv expr =
  let newVarNames = ref curEnv.variables in
  let rec addAllAssign (expr : expr) =
    match expr.desc with
    | Ebinop (e1, _, e2) ->
        addAllAssign e1;
        addAllAssign e2
    | Eunop (_, e)
    | Epard_ident (e, _)
    | Ereturn (Some e)
    | Elvalue (LvalueStruct (e, _)) ->
        addAllAssign e
    | Ebloc b | Eentier_parg_bloc (_, b) | Ecall (_, b) ->
        List.iter addAllAssign b
    | Efor (_, e1, e2, b) ->
        addAllAssign e1;
        addAllAssign e2;
        if recurse then List.iter addAllAssign b
    | Eif (cond, b1, b2) ->
        addAllAssign cond;
        List.iter addAllAssign b1;
        List.iter addAllAssign b2
    | Ewhile (cond, b) ->
        addAllAssign cond;
        if recurse then List.iter addAllAssign b
    | Eassign (LvalueIdent id, _) ->
        if takeNothing || id <> "nothing" then
          newVarNames := Smap.add id Any !newVarNames
    | _ -> ()
  in
  addAllAssign expr;
  { curEnv with variables = !newVarNames }

let rec modReturns name expr =
  match expr.desc with
  | Treturn (x, _) ->
      if name = "" then
        raise (TypingError (expr.loc, "Return outside of function."));
      { expr with desc = Treturn (x, name) }
  | Tcst _ | Tident _ | Tentier_ident _ | Tlvalue (TLvalueIdent _) -> expr
  | Tbinop (t1, op, t2) ->
      { expr with desc = Tbinop (modReturns name t1, op, modReturns name t2) }
  | Tunop (unop, t) -> { expr with desc = Tunop (unop, modReturns name t) }
  | Tbloc l -> { expr with desc = Tbloc (List.map (modReturns name) l) }
  | Tpard_ident (t, id) ->
      { expr with desc = Tpard_ident (modReturns name t, id) }
  | Tentier_parg_bloc (x, l) ->
      { expr with desc = Tentier_parg_bloc (x, List.map (modReturns name) l) }
  | Tcall (id, l) ->
      { expr with desc = Tcall (id, List.map (modReturns name) l) }
  | Tstructure (s, l) ->
      { expr with desc = Tstructure (s, List.map (modReturns name) l) }
  | Tfor (id, t1, t2, l, inside) ->
      {
        expr with
        desc =
          Tfor
            ( id,
              modReturns name t1,
              modReturns name t2,
              List.map (modReturns name) l, inside );
      }
  | Twhile (t, l, inside) ->
      {
        expr with
        desc = Twhile (modReturns name t, List.map (modReturns name) l, inside);
      }
  | Tif (t1, l1, l2) ->
      {
        expr with
        desc =
          Tif
            ( modReturns name t1,
              List.map (modReturns name) l1,
              List.map (modReturns name) l2 );
      }
  | Tlvalue (TLvalueStruct (typ, t, id)) ->
      { expr with desc = Tlvalue (TLvalueStruct (typ, modReturns name t, id)) }
  | Tassign (TLvalueStruct (typ, t1, id), t2) ->
      {
        expr with
        desc =
          Tassign
            (TLvalueStruct (typ, modReturns name t1, id), modReturns name t2);
      }
  | Tassign (x, t) -> { expr with desc = Tassign (x, modReturns name t) }
