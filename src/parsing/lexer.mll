
(* Analyseur lexical pour le langage Julia *)

{
  open Parser
  open Lexing

  let keywords =
    [
      "true", TRUE;
      "false", FALSE;
      "return", RETURN;
      "for", FOR;
      "end", END;
      "while", WHILE;
      "else", ELSE;
      "elseif", ELSEIF;
      "if", IF;
      "function", FUNCTION;
      "mutable", MUTABLE;
      "struct", STRUCT
    ]

  exception Lexing_error of string

  let hsh = Hashtbl.create 16
  let create_hash = List.iter (fun (a, b) -> Hashtbl.add hsh a b) keywords
  
  (* Décide si l'ident est un identifiant ou un mot clé *)

  let kwd_or_ident s = try Hashtbl.find hsh s with | Not_found -> IDENT s
  
  (* S'occupe de transformer une chaine représentant un entier en un entier Int64, le "0u" permet de considérer l'entier comme non signé, et peut donc aller jusque 2^64 - 1 *)

  let get_int s = 
    try 
      let ret = Int64.of_string ("0u" ^ s) in
      if Int64.unsigned_compare ret Int64.min_int > 0 then failwith "" 
      else ret 
    with _ -> raise (Lexing_error ("integer too big : " ^ s ^ "."))

  (* Transorme une chaine de la forme entier ident en une paire entier, ident,
   * et vérifie également que l'ident n'est pas un mot clé ! *)

  let get_entier_ident s =
    let i = ref 0 in
    while (Char.code s.[!i]) >= (Char.code '0') && (Char.code s.[!i]) <= (Char.code '9') do
      incr i
    done;
    let entier = get_int (String.sub s 0 !i) in
    let id = String.sub s !i (String.length s - !i) in
    if Hashtbl.mem hsh id then
      raise (Lexing_error (id ^ " is a reserved keyword"));
    (entier, id)

  (* S'occupe du formatage d'une chaine de caractères, 
   * afin de transformer les \n présents en retour chariot entre autre *)

  let get_string s = 
    let s = String.sub s 1 (String.length s - 2) in
    let rec createList = function
      | [] -> []
      | '\\' :: '\\' :: q -> '\\' :: (createList q)
      | '\\' :: '"' :: q -> '"' :: (createList q)
      | '\\' :: 'n' :: q -> '\n' :: (createList q)
      | '\\' :: 't' :: q -> '\t' :: (createList q)
      | c :: q -> c :: (createList q)
   in String.of_seq (List.to_seq (createList (List.of_seq (String.to_seq s))))

}

let space = [' ' '\t']
let chiffre = ['0'-'9']
let alpha = ['a'-'z' 'A'-'Z' '_']
let entier = chiffre+
let car = [' ' '!' '#'-'[' ']'-'~'] | "\\\\" | "\\\"" | "\\n" | "\\t"
let chaine = '"' car* '"'
let ident = alpha (chiffre | alpha)*
let entier_ident = entier ident
let ident_parg = ident '('
let entier_parg = entier '('
let pard_ident = ')' ident

rule next_tokens = parse
  | space+
    { next_tokens lexbuf }
  | '#' [^ '\n']* '\n' | '\n'
    { new_line lexbuf; NEWLINE }
  | entier as s
    { CST (get_int s) }
  | '+' { ADD } | '-' { SUB } | '*' { MUL } | '%' { MOD } | '^' { POW }
  | "==" { EQ } | "!=" { DIFF } | '<' {  LT  } | "<=" {  LE  } | '>' {  GT  } | ">=" {  GE  }
  | "&&"  {  AND  } | "||" {  OR  } 
  | entier_ident as s
    { ENTIER_IDENT (get_entier_ident s) }
  | ident_parg as s
    { IDENT_PARG (String.sub s 0 (String.length s - 1)) }
  | entier_parg as s
    { let s = String.sub s 0 (String.length s - 1) in ENTIER_PARG (get_int s) }
  | pard_ident as s
    { PARD_IDENT (String.sub s 1 (String.length s -1)) }
  | '('
    { LPAR }
  | ')'
    { RPAR }
  | ';'
    { SEMICOLON }
  | ','
    { COMMA }
  | "::"
    { DOUBLECOLON }
  | ":"
    { COLON }
  | '.'
    { DOT }
  | '='
    { ASSIGN }
  | '!'
    { NOT }
  | chaine as s
    { STRING (get_string s) }
  | ident as s
    { kwd_or_ident s } 
  | eof
    { EOF }
  | _ as c
    { raise (Lexing_error ("Illegal character: " ^ String.make 1 c ^ ".")) }

{
  (* Fonction qui à partir d'un lexbuf, renvoie une fonction qui s'occupe de renvoyer un token.
   * Elle gère entre autre l'ajout de SEMICOLON lorsque il y a un retour chariot, et aussi la vérification pour la suite IF ELSE
   *)
  let next_token =
    let tokens = Queue.create () in
    let lastToken = ref EOF in
    let tokensColon = function
      | IDENT _ | CST _ | ENTIER_IDENT _ | PARD_IDENT _ | TRUE | FALSE | RPAR 
      | END | STRING _ | RETURN  -> true
      | _ -> false
    in

    let rec ret lb = 
      if Queue.is_empty tokens then Queue.add (next_tokens lb) tokens;
      let curToken = Queue.pop tokens in
      match curToken with
        | NEWLINE -> 
            ( if tokensColon !lastToken then (
            lastToken := SEMICOLON;
            SEMICOLON
           ) else ret lb)
       | IF ->
           if !lastToken = ELSE then raise (Lexing_error "invalid use of else if : use elseif instead.")
           else ( lastToken := IF; IF)
       | _ ->
          lastToken := curToken;
          curToken
    in ret
}
