(*
 * Arbre de syntaxe abstraire pour Julia
 *)

type loc = Lexing.position * Lexing.position

type ident = string

type typ = Any | Nothing | Int64 | Bool | String | Structure of ident

let string_to_typ = function
  | "Any" -> Any
  | "Nothing" -> Nothing
  | "Int64" -> Int64
  | "Bool" -> Bool
  | "String" -> String
  | s -> Structure s

let typ_to_string = function
  | Any -> "any"
  | Nothing -> "nothing"
  | Int64 -> "int"
  | Bool -> "bool"
  | String -> "string"
  | Structure s -> "structure_" ^ s

type unop = Uneg | Unot

type binop =
  | Beq
  | Bdiff
  | Blt
  | Ble
  | Bgt
  | Bge
  | Badd
  | Bsub
  | Bmul
  | Bmod
  | Bpow
  | Band
  | Bor

type cst = Cint of int64 | Cstring of string | Cbool of bool

type bloc = expr list

and lvalue = LvalueIdent of ident | LvalueStruct of expr * ident

and expr = { desc : exprDesc; loc : loc }

and exprDesc =
  | Ecst of cst
  | Eident of ident
  | Ebinop of expr * binop * expr
  | Eunop of unop * expr
  | Ebloc of bloc
  | Eentier_ident of int64 * ident
  | Epard_ident of expr * ident
  | Eentier_parg_bloc of int64 * bloc
  | Ecall of ident * expr list
  | Ereturn of expr option
  | Efor of ident * expr * expr * bloc
  | Ewhile of expr * bloc
  | Eif of expr * bloc * bloc
  | Elvalue of lvalue
  | Eassign of lvalue * expr

type param = { desc : ident * typ; loc : loc }

type fonction = {
  name : ident;
  params : param list;
  retType : typ;
  bloc : bloc;
  loc : loc;
}

type structure = {
  isMutable : bool;
  name : ident;
  params : param list;
  loc : loc;
}

type decl = Dexpr of expr | Dfun of fonction | Dstruct of structure

type fichier = decl list

module Sset = Set.Make (String)
module Smap = Map.Make (String)

type environment = {
  structures : structure Smap.t;
  fieldNames : structure Smap.t;
  functions : fonction list Smap.t;
  variables : typ Smap.t;
}

type typedLvalue =
  | TLvalueIdent of typ * ident
  | TLvalueStruct of typ * typedExpr * ident

and typedBloc = typedExpr list

and typedExpr = { desc : typedExprDesc; loc : loc; typ : typ }

and typedExprDesc =
  | Tcst of cst
  | Tident of ident
  | Tbinop of typedExpr * binop * typedExpr
  | Tunop of unop * typedExpr
  | Tbloc of typedBloc
  | Tentier_ident of int64 * ident
  | Tpard_ident of typedExpr * ident
  | Tentier_parg_bloc of int64 * typedBloc
  | Tcall of ident * typedExpr list
  | Tstructure of structure * typedExpr list
  | Treturn of typedExpr option * string
  | Tfor of ident * typedExpr * typedExpr * typedBloc * environment
  | Twhile of typedExpr * typedBloc * environment
  | Tif of typedExpr * typedBloc * typedBloc
  | Tlvalue of typedLvalue
  | Tassign of typedLvalue * typedExpr

type typedFunction = {
  name : ident;
  params : param list;
  retType : typ;
  bloc : typedBloc;
  loc : loc;
  localEnv: typ Smap.t;
}

type typedFile = {
  globalEnv : environment;
  exprs : typedExpr list;
  functions : typedFunction list Smap.t;
  structures : structure Smap.t;
}
