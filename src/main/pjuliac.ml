(* Programme principal *)

open Format
open Lexing
open Parser
open Ast
open Typing
open Production

let usage = "usage : pjuliac [options] file.jl"

let parse_only = ref false

let type_only = ref false

let spec =
  [
    ("--parse-only", Arg.Set parse_only, " stop after parsing");
    ("--type-only", Arg.Set type_only, " stop after typing");
  ]

let file =
  let file = ref None in
  let set_file s =
    if not (Filename.check_suffix s ".jl") then
      raise (Arg.Bad "no .jl extension");
    file := Some s
  in
  Arg.parse spec set_file usage;
  match !file with
  | Some f -> f
  | None ->
      Arg.usage spec usage;
      exit 1

let report (b, e) =
  let l = b.pos_lnum in
  let fc = b.pos_cnum - b.pos_bol + 1 in
  let lc = e.pos_cnum - b.pos_bol in
  eprintf "File \"%s\", line %d, characters %d-%d:\n" file l fc lc

let () =
  let c = open_in file in
  let lb = Lexing.from_channel c in
  try
    let f = Parser.file Lexer.next_token lb in
    close_in c;
    if !parse_only then exit 0;
    let typedFile = typeFile f in
    if !type_only then exit 0;
    let shrinked_file = String.sub file 0 (String.length file - 3) in
    X86_64.print_in_file (shrinked_file ^ ".s") (compile typedFile)
  with
  | Lexer.Lexing_error s ->
      report (lexeme_start_p lb, lexeme_end_p lb);
      eprintf "lexical error : %s@." s;
      exit 1
  | Parser.Error ->
      report (lexeme_start_p lb, lexeme_end_p lb);
      eprintf "syntax error@.";
      exit 1
  | Typing_util.TypingError (pos, s) ->
      report pos;
      eprintf "typing error : %s@." s;
      exit 1
  | e ->
      eprintf "Anomaly : %s\n" (Printexc.to_string e);
      exit 2
