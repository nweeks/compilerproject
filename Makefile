all: pjuliac

test: pjuliac
	./pjuliac tests/test.jl
	gcc -no-pie tests/test.s -o tests/test.out
	@echo "FINISHED COMPILING"
	./tests/test.out


tests: test_syntax test_typing test_exec

test_syntax: pjuliac
	bash tests/test.sh -1 "./pjuliac --parse-only"

test_typing: pjuliac
	bash tests/test.sh -2 "./pjuliac --type-only"

test_exec: pjuliac
	bash tests/test.sh -3 "./pjuliac"

format:
	dune build @fmt
	@echo ""
	dune promote

pjuliac:
	dune build
	@echo ""
	@cp -f _build/default/pjuliac.exe pjuliac

clean:
	@dune clean
	@rm -f pjuliac
	@rm -f tests/test.out
	@rm -f tests/test.s
	@rm -f pjuliac
	@rm -f tests/exec/*.s tests/exec-fail/*.s
	@rm -f a.out out

.PHONY: all clean pjuliac tests test_syntax test_typing
