# compilerProject

Compilateur pour le langage Mini-Julia, réalisé dans le cadre du projet du cours de compilation de L3 à l'ENS.

- Pour compiler les sources, la commande make suffit, et créé un éxécutable pjuliac à la source du projet
- Pour lancer les tests unitaires, les commandes "make tests", "make test_syntax" "make test_typing" et "make test_exec" conviennent, la commande "make tests" étant équivalente à l'éxecution de tous les tests.
- Pour tester sur un fichier, éditer le fichier tests/test.jl puis lancer "make test" convient

Pour le moment, le compilateur réalise l'analyse lexicale, syntaxique, le typage, et peut compiler tout sauf les structures qui sont pour le moment en cours de développement.

Tous les types, et en particulier les arbres de syntaxes abstraite utilisés sont disponibles dans le fichier "src/trees/ast.ml". Il n'y a rien de particulièrement intéressant, si ce n'est qu'on utilise un arbre différent pour les expression/fonctions typées ou non.

Les analyses  lexicales et syntaxiques utilisent respectivement ocamllex et menhir, les sources peuvent être trouvées dans src/parsing. Les commentaires dans "src/parsing/lexer.mll" devraient suffire à la compréhension du code.

Pour l'analyse syntaxique, le point important est la facon dont est géré la construction expr bloc présente dans les constructeurs if, while et for. On utilise la convention de julia qui prend la plus grande expression possible. Ainsi, si le bloc commence par une expression, celle-ci ne peut pas commencer par SUB expr, c'est ainsi qu'on sépare les productions en 3 : les neutres, celles qui peuvent commencer par SUB expr, et celles qui ne peuvent pas.
Le reste ne devrait pas poser de soucis.

Pour le typage, nous avons suivi à la lettre le sujet, en essayant aussi de trouver des erreurs de dispatch multiple lors du typage, à l;aide des fonctions disponibles dans "src/typing/typing_util.ml". Le typage change aussi les noms des fonctions, on concatène à chaque fonction son nombre d'arguments, et on concatène ensuite à ce nom un entier la distinguant des autres fonctions qui ont le mêmême nom et le même nombre d'arguments. Ainsi, le typage renvoie un environnement avec l'environnement global, la liste des expressions typées dans l'ordre d'éxecution,  une map qui à un identifiant de fonction on renvoie toutes les fonctions typées avec le même nombre d'argument, et enfin l'ensemble des structures.
Pour le moment, la distinction entre appel de fonction, et constructeur de structure se fait naivement : on ne peut avoir une fonction et une structure qui ont le même nom. Mais ceci va être amélioré et intégré dans le dispatch multiple bientôt : On ne pensait pas que ce serait facile de le faire mais finalement c'est faisable.
Pour la gestion des variables locales dans les boucles for/while, la fonction addAssign s'en occupe, elle prend en particulier deux paramètres supplémentaires pour savoir si il faut ajouter récursivement les variables affectées dans les boucles ou non. Ceci permet lors du typage de vérifier les portées, puis d'avoir toutes les variables locales lors de la production de code. En revanche, ceci sera modifié quand on aura avancé la production de code, car ceci considère comme global les variables locales à une boucle, et ne permet donc pas de bien faire la vérification dynamique de définition. Mais ceci changera pas les programmes qu'on accepte au typage.

Le code du typage est malheureusement assez peu lisible car très long et un peu répetitif, bon courage pour la lecture, nous avons essayé de séparer en deux fichiers, en gardant les fonctions auxiliaire dans le fichier typing_util.ml.

